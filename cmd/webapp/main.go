package main //webapp

import (
	"fmt"
	"net/http"

	"google.golang.org/appengine"
)

func webapp(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "webapp")
}

func main() {
	http.HandleFunc("/", webapp)
	http.ListenAndServe(":8080", nil)

	appengine.Main()
}
