package main //clock

import (
	"fmt"
	"net/http"

	"bitbucket.org/sidthbigsloth/assignment2-cloud/src/DB"

	"google.golang.org/appengine"
)

func clock(w http.ResponseWriter, r *http.Request) {
	//for true {
	DB.Update(w, r)
	//time.Sleep(60 * time.Second)
	//}
}

func test(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "test")
}

func main() {
	http.HandleFunc("/", clock)
	http.HandleFunc("/clock", clock)
	http.ListenAndServe(":8080", nil)

	appengine.Main()
}
