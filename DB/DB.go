package DB

import (
	"encoding/json"
	"fmt"
	"net/http"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
	mgo "mgo.v2"
)

type currency struct {
	Base  string `json:"base"`
	Date  string `json:"date"`
	Rates struct {
		AUD float32 `json:"AUD"`
		BGN float32 `json:"BGN"`
		BRL float32 `json:"BRL"`
		CAD float32 `json:"CAD"`
		CHF float32 `json:"CHF"`
		CNY float32 `json:"CNY"`
		CZK float32 `json:"CZK"`
		DKK float32 `json:"DKK"`
		EUR float32 `json:"EUR"`
		GBP float32 `json:"GBP"`
		HKD float32 `json:"HKD"`
		HRK float32 `json:"HRK"`
		HUF float32 `json:"HUF"`
		IDR float32 `json:"IDR"`
		ILS float32 `json:"ILS"`
		INR float32 `json:"INR"`
		JPY float32 `json:"JPY"`
		KRW float32 `json:"KRW"`
		MXN float32 `json:"MXN"`
		MYR float32 `json:"MYR"`
		NOK float32 `json:"NOK"`
		NZD float32 `json:"NZD"`
		PHP float32 `json:"PHP"`
		PLN float32 `json:"PLN"`
		RON float32 `json:"RON"`
		RUB float32 `json:"RUB"`
		SEK float32 `json:"SEK"`
		SGD float32 `json:"SGD"`
		THB float32 `json:"THB"`
		TRY float32 `json:"TRY"`
		USD float32 `json:"USD"`
		ZAR float32 `json:"ZAR"`
	} `josn:"rates"`
}

//Update .
func Update(w http.ResponseWriter, r *http.Request) {
	var C currency
	usr := "test"
	pswrd := "test"
	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	base := "EUR"
	url := "http://api.fixer.io/latest?base="
	mongoURL := "mongodb://" + usr + ":" + pswrd + "@ds243085.mlab.com:43085/assignment2-cloud"
	//testDB = "@ds249355.mlab.com:49355/assignment2-cloud-test"
	//DB = "@ds243085.mlab.com:43085/assignment2-cloud"
	fmt.Fprint(w, mongoURL)
	url += base
	page, err := client.Get(url)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	json.NewDecoder(page.Body).Decode(&C)

	fmt.Fprint(w, C)

	session, err := mgo.Dial(mongoURL)

	if err != nil {
		panic(err)
	}

	//defer session.Close()

	session.SetMode(mgo.Strong, true)

	collection := session.DB("assignment2-cloud").C("test")

	index := mgo.Index{
		Key:        []string{"base", "date"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = collection.EnsureIndex(index)
	if err != nil {
		panic(err)
	}

	err = collection.Insert(&C)
	if err != nil {
		//panic(err)
	}
	//*/

	session.Close()
}
